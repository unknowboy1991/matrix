## Summary
<!-- 
Note: If you're a website owner that has been specifically targeted, fix the 
site before reporting. Remove revolving ad servers, popup ads, adblock 
countering etc. Only then will this request be reviewed. -->

<!-- Summarize the reason encountered concisely, and keep any domains in 
back ticks `(`)` -->

This domain Should be whitelist be curse .. I have A seriously damned 
good reason

- [X] <a href="source/whitelist/domains.list">Single Domain</a>
- [ ] <a href="source/whitelist/wildcard.list">Wild carded</a>

```python
example.com   CNAME . ; whitelisted 
*.example.com   CNAME . ; whitelisted 
```

# Extravagant good reason
<!-- Try to convince the team of why this domain should be added to the 
whitelist -->

## Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format 
console output, logs, and code as it's very hard to read otherwise. -->



### All Submissions:
- [ ] Have you followed the guidelines in our [Contributing](CONTRIBUTING.md) document?
- [ ] Have you checked to ensure there aren't other open
	[Merge Requests (MR)](../merge_requests) or [Issues](../issues) for
	the same update/change?
- [ ] Added ScreenDump for prove of False Positive
- [ ] Have you added an explanation of what your submission do and why
	you'd like us to include them??

### Testing face
- [ ] Checked the internet for verification?
- [ ] Have you successfully ran tests with your changes locally?

### Todo:
- [ ] RPZ Server
- [ ] Added to Source file

/label ~Whitelist  
/assign @AnonymousPoster @Spirillen
/estimate 15m
/weight 2
